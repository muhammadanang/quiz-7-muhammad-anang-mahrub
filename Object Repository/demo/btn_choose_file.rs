<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_choose_file</name>
   <tag></tag>
   <elementGuidId>11f33b05-3027-4d46-9f1a-153e1ec972fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'file-upload']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file-upload</value>
      <webElementGuid>15558abd-865b-4945-95a2-9f4b8a2ab933</webElementGuid>
   </webElementProperties>
</WebElementEntity>
